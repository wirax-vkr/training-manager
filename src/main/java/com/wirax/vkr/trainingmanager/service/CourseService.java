package com.wirax.vkr.trainingmanager.service;

import com.wirax.vkr.trainingmanager.model.Course;
import com.wirax.vkr.trainingmanager.model.Training;
import com.wirax.vkr.trainingmanager.repository.CourseRepository;
import com.wirax.vkr.trainingmanager.repository.TrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class CourseService {
    private final CourseRepository repository;

    @Autowired
    public CourseService(CourseRepository repository) {
        this.repository = repository;
    }

    public Course create(Course course) {
        return repository.save(course);
    }

    public Course update(UUID id, Course course) {
        if(id != course.getId()) {
            throw new IllegalArgumentException("Wrong parameter id");
        }
        return repository.save(course);
    }

    public List<Course> findAll() {
        return repository.findAll();
    }

    public void delete(UUID id) {
        repository.deleteById(id);
    }
}
