package com.wirax.vkr.trainingmanager.service;

import com.wirax.vkr.trainingmanager.model.Training;
import com.wirax.vkr.trainingmanager.repository.TrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TrainingService {
    private final TrainingRepository repository;

    @Autowired
    public TrainingService(TrainingRepository repository) {
        this.repository = repository;
    }

    public Training create(Training document) {
        return repository.save(document);
    }

    public Training update(UUID id, Training document) {
        if(id != document.getId()) {
            throw new IllegalArgumentException("Wrong parameter id");
        }
        return repository.save(document);
    }

    public List<Training> findAll() {
        return repository.findAll();
    }

    public void delete(UUID id) {
        repository.deleteById(id);
    }
}
