package com.wirax.vkr.trainingmanager.controller;

import com.wirax.vkr.trainingmanager.mapper.TrainingMapper;
import com.wirax.vkr.trainingmanager.mapper.TrainingMapperImpl;
import com.wirax.vkr.trainingmanager.model.dto.TrainingDto;
import com.wirax.vkr.trainingmanager.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController("/training")
public class TrainingController {
    public final TrainingService service;
    private TrainingMapper mapper = new TrainingMapperImpl();

    @Autowired
    public TrainingController(TrainingService service) {
        this.service = service;
    }

    @PostMapping("/training")
    public TrainingDto create(@RequestBody TrainingDto statement) {
        return mapper.toDto(service.create(mapper.toEntity(statement)));
    }

    @PutMapping("/training")
    public TrainingDto update(@RequestParam UUID id, @RequestBody TrainingDto user) {
        return mapper.toDto(service.update(id, mapper.toEntity(user)));
    }

    @GetMapping("/training")
    public List<TrainingDto> findAll() {
        return service.findAll()
                .stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/training")
    public void delete(@RequestParam UUID id) {
        service.delete(id);
    }
}
