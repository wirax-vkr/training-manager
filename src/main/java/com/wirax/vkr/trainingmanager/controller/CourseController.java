package com.wirax.vkr.trainingmanager.controller;

import com.wirax.vkr.trainingmanager.mapper.CourseMapper;
import com.wirax.vkr.trainingmanager.mapper.CourseMapperImpl;
import com.wirax.vkr.trainingmanager.model.dto.CourseDto;
import com.wirax.vkr.trainingmanager.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class CourseController {
    public final CourseService service;
    private CourseMapper mapper = new CourseMapperImpl();

    @Autowired
    public CourseController(CourseService service) {
        this.service = service;
    }

    @PostMapping("/course")
    public CourseDto create(@RequestBody CourseDto statement) {
        return mapper.toDto(service.create(mapper.toEntity(statement)));
    }

    @PutMapping("/course")
    public CourseDto update(@RequestParam UUID id, @RequestBody CourseDto user) {
        return mapper.toDto(service.update(id, mapper.toEntity(user)));
    }

    @GetMapping("/course")
    public List<CourseDto> findAll() {
        return service.findAll()
                .stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/course")
    public void delete(@RequestParam UUID id) {
        service.delete(id);
    }
}
