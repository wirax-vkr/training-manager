package com.wirax.vkr.trainingmanager.repository;

import com.wirax.vkr.trainingmanager.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CourseRepository extends JpaRepository<Course, UUID> {
}
