package com.wirax.vkr.trainingmanager.repository;

import com.wirax.vkr.trainingmanager.model.Training;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface TrainingRepository extends JpaRepository<Training, UUID> {
}
