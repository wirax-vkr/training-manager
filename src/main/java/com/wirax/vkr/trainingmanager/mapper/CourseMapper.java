package com.wirax.vkr.trainingmanager.mapper;

import com.wirax.vkr.trainingmanager.model.Course;
import com.wirax.vkr.trainingmanager.model.dto.CourseDto;
import org.mapstruct.Mapper;

@Mapper
public interface CourseMapper {
    Course toEntity(CourseDto dto);

    CourseDto toDto(Course entity);
}
