package com.wirax.vkr.trainingmanager.mapper;

import com.wirax.vkr.trainingmanager.model.Training;
import com.wirax.vkr.trainingmanager.model.dto.TrainingDto;
import org.mapstruct.Mapper;

@Mapper
public interface TrainingMapper {
    Training toEntity(TrainingDto dto);

    TrainingDto toDto(Training entity);
}
