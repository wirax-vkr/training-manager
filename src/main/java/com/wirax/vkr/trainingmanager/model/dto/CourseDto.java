package com.wirax.vkr.trainingmanager.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseDto {
    private UUID id;
    private List<UUID> subscribers;
    private UUID trainingManager;
}
