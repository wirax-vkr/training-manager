package com.wirax.vkr.trainingmanager.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrainingDto {
    private UUID id;
    private OffsetDateTime dateLink;
    private String additionalInfo;
    private UUID skill;
}
